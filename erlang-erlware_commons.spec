%global realname erlware_commons
%global upstream erlware

# Technically we're noarch, but our install path is not. For non-NIF
%global debug_package %{nil}

Name:     erlang-%{realname}
Version:  1.2.0
Release:  1%{?dist}
Summary:  Extension to Erlang's standard library
License:  MIT
URL:      https://github.com/%{upstream}/%{realname}
Source0:  https://github.com/%{upstream}/%{realname}/archive/%{version}/%{realname}-%{version}.tar.gz
# The "color" test does not play well with Fedora's build system
Patch0:   erlang-erlware_commons-disable-color_test.patch
BuildRequires:  erlang-rebar
BuildRequires:  erlang-cf
Requires:       erlang-rebar
Requires:       erlang-cf

%description
%{summary}.

%prep
%setup -q -n %{realname}-%{version}
%patch0

%build
%{erlang_compile}

%install
%{erlang_install}
cp -arv priv/ %{buildroot}%{erlang_appdir}/

%check
%{erlang_test}

%files
%license COPYING
%doc doc/ README.md
%{erlang_appdir}/

%changelog
* Sat Jul 14 2018 Timothée Floure <fnux@fedoraproject.org> - 1.2.0-1
- Let there be package
